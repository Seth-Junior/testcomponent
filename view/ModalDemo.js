import React from 'react'
import { Button,View, Text, Modal, Image, StyleSheet } from 'react-native'

const ModalDemo = (props) => {

    function onClose(){
        props.onParentClose(false)
    }

    return (
        <Modal visible={props.isShow} animationType='slide' > 
            <View>
            <Image style={styles.imageCSS} source={require('../assets/image1.jpeg')}/>
            <Button title="Close" onPress={onClose}/>
            </View>
        </Modal>
    )
}

export default ModalDemo


const styles = StyleSheet.create({
    imageCSS: {
        width: '100%',
        height: 400
    }
})