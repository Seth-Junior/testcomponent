import React, { useCallback } from "react";
import { Alert, Button, Linking, View } from "react-native";

const linkingComponent = "https://reactnative.dev/docs/linking";

const OpenURLButton = ({ url, children }) => {
  const handlePress = useCallback(async () => {
    // Checking if the link is supported for links with custom URL scheme.
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  }, [url]);

  return <Button title={children} onPress={handlePress} />;
};

const LinkingCom = () => {
  return (
    <View>
      <OpenURLButton url={linkingComponent} >Go to React Native Linking Component</OpenURLButton>
    </View>
  );
};


export default LinkingCom;