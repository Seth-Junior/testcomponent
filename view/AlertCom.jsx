import React from "react";
import { View, Button, Alert } from "react-native";

const AlertCom = () => {
    const createButtonAlert = () =>
        Alert.alert(
            "Alert Title",
            "My Alert Msg",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
        );

    return (
        <View>
            <Button title={"Alert"} onPress={createButtonAlert} />
        </View>
    )
}

export default AlertCom

