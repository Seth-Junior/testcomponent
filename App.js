import React from 'react'
import { View , StyleSheet } from 'react-native'
import ActivityIndicatorCom from './view/ActivityIndicatorCom';
import ActivityIndicators from './view/ActivityIndicators';
import AlertCom from './view/AlertCom';
import AnimatedCom from './view/AnimatedCom';
import FlatListDemo from './view/FlatlistDemo';
import ImageAPI from './view/ImageAPI';
import KeyboardAvoidingComponent from './view/KeyboardAvoidingComponent';
import LinkingCom from './view/LinkingCom';
import ModalCom from './view/ModalCom';
import NestComponent from './view/NestComponent';
import States from './view/States';

const App = () => {
  return (
    <View style={style.container}>
      {/* <AnimatedCom /> */}
      {/* <ActivityIndicatorCom/> */}
      {/* <AlertCom/> */}
      {/* <KeyboardAvoidingComponent /> */}
      {/* <LinkingCom />  */}
      {/* <NestComponent /> */}
      {/* <ModalCom /> */}
      {/* <States />
      <View style={style.line}/>
      <ActivityIndicators /> */}
      <ImageAPI/>
      {/* <FlatListDemo/> */}
    </View>
  )
}

export default App;

const style = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  line:{
    width:'90%',
    height:1,
    backgroundColor:'black',
    margin:5
  }
})