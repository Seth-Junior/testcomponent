import React, {useState} from 'react'
import { View, Text, Image, TouchableOpacity, Alert, TouchableHighlight, StyleSheet } from 'react-native'
import ModalDemo from './ModalDemo'


const ImageAPI = () => {
    const [isShow, setIsShow] = useState(false)

    function onShowModal(){
        setIsShow(true)
    }

    function onParentClose(vb){
        setIsShow(vb)
    }

    return (
        <>
            <ModalDemo isShow={isShow} onParentClose={()=>onParentClose(false)}/> 
            <TouchableHighlight style={styles.container} onPress={onShowModal}>
                <Image style={{ width: '100%', height: 200 }} source={require('../assets/image1.jpeg')} />

                {/* <Text>Click</Text> */}
            </TouchableHighlight>
        </>
    )
}

export default ImageAPI

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 200
    }
})