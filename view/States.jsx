import React,{useState} from 'react'
import { Text, View , Button } from 'react-native'

const Person = (props) => {

    const [isClick, setIsClick] = useState(true);

    return (
        <View>
            <Text style={{color:'red',margin:10}}>Passing data by state and props </Text>
            <Text>
                Hello {props.name}, {isClick ? "pleased Click me !" : "Clicked !"}
            </Text>
            <Button
                onPress={() => { setIsClick(false); }}
                color="#f194ff"
                title={isClick ? "Click Me !" : "Clicked !"}
            />
        </View>
    );
}


const States = () => {
    return (
        <View>
            <Person name="Kimhak" />
        </View>
    )
}

export default States

